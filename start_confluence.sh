#!/bin/bash

# 启动容器前你需要创建一个工作目录映射到docker使用.这样可以在后面升级和替换docker的时候可以数据不丢失.
# 我这里使用的工作目录为: /home/fixme/confluence
# 为了不必要的权限 问题. 可以把此目录权限 修改为777

if [[ ! -d /home/fixme/confluence ]]; then
  sudo mkdir -p /home/fixme/confluence && \
  sudo chmod 777 -R /home/fixme/confluence &&\
  sudo chown 1001:docker -R  /home/fixme/confluence
  retVal=$?
  if [[ ${retVal} -ne 0 ]]; then
    echo "创建数据目录失败:[code]${retVal}"
    exit 1
  fi
fi


if [[ ! -d /home/fixme/mysql ]]; then

  sudo mkdir -p /home/fixme/mysql
  sudo mkdir -p /home/fixme/mysql/bitnami-mysql-conf
  sudo mkdir -p /home/fixme/mysql/bitnami-mysql-data

  # 复制配置文件
  sudo cp -rf ./bitnami-mysql-conf/my_custom.cnf /home/fixme/mysql/bitnami-mysql-conf/my_custom.cnf

  # 权限处理
  sudo chmod 777         -R /home/fixme/mysql
  sudo chown 1001:docker -R /home/fixme/mysql
  retVal=$?
  if [[ ${retVal} -ne 0 ]]; then
    echo "创建数据目录失败:[code]${retVal}"
    exit 1
  fi
fi

#  ------------------  启动mysql ------------------

echo "启动mysql容器"

docker ps | grep mysql-8.0.28 |  grep 8.0.28-debian-10-r81 -q

if [[ $? -eq 0 ]];then
  echo "已经存在Docker镜像: 8.0.28-debian-10-r81"
else
  docker run -d \
    -p 13306:3306 \
    --name mysql-8.0.28 \
    -e ALLOW_EMPTY_PASSWORD=yes \
    -e MYSQL_ROOT_PASSWORD=123456 \
    -e MYSQL_CHARACTER_SET=utf8mb4 \
    -e MYSQL_COLLATION=utf8mb4_bin \
    -e MYSQL_ENABLE_SLOW_QUERY=1 \
    -e MYSQL_LONG_QUERY_TIME=5.0 \
    -e MYSQL_USER=conf_user \
    -e MYSQL_PASSWORD=conf_password \
    -e MYSQL_DATABASE=conf_db \
    -e TZ=Asia/Shanghai \
    -v /home/fixme/mysql/bitnami-mysql-data:/bitnami/mysql/data \
    -v /home/fixme/mysql/bitnami-mysql-conf/my_custom.cnf:/opt/bitnami/mysql/conf/my_custom.cnf:ro \
    bitnami/mysql:8.0.28-debian-10-r81
    echo "启动完成, 启动结果:$?"
fi




# ------------------  启动confluence ------------------


localIP=$(ip addr | grep inet | grep -v inet6 | grep -v 127 | grep -v 172 | awk '{print $2}' | grep -P -o '\d+.\d+.\d+.\d+' | head -n 1)
localIP="${localIP:-unknown}"

echo "注意: 请先启动mysql容器，再启动wiki容器"
echo "1. 如果是首次安装,请不要使用nginx反向代理. 直接使用ip:port访问,并且使用http协议. 访问地址: http://${localIP}:28080"
echo "2. 如果是已经安装过的,请使用nginx反向代理,并且使用https协议,您可以访问: https://your.wiki.com (这个要先初始化ng,并申请好证书)"
echo "启动脚本都以https方式的参数启动容器.这并不影响你使用IP直接访问它."
echo "如果要调整confluence的JVM内存. 使用命令: docker exec -it confluence-8.5.2 /bin/bash 进入容器,修改/opt/atlassian/confluence/bin/setenv.sh文件"


echo "启动confluence容器"
docker run -d --name confluence-8.5.2 \
  --restart always \
  --link mysql-8.0.28 \
  -p 28080:8090 \
  -e TZ="Asia/Shanghai" \
  -e X_PROXY_NAME="$localIP" \
  -e X_PROXY_PORT="28080" \
  -e X_PROXY_SCHEME="http" \
  -v /home/fixme/confluence8:/var/atlassian/confluence \
  confluence:8.5.2-JDK17-agent-chs

echo "启动完成, 启动结果:$?"

# jdbc:mysql://mysql-8.0.28:3306/conf_db?useUnicode=true&characterEncoding=utf8


