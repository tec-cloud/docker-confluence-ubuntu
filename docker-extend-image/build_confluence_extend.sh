#!/bin/bash

if [ ! -f "atlassian-agent.jar" ]; then
    echo 'atlassian-agent.jar not found, downloading...'
    AGENT_VERSION=1.3.3
    AGENT_PATH=.
    AGENT_FILENAME='atlassian-agent.jar'
    # md5 = 212ae1ccf5599addb2ccd06266910a9c , you can check it by yourself
    curl -o ${AGENT_PATH}/${AGENT_FILENAME}  https://github.com/haxqer/confluence/releases/download/v${AGENT_VERSION}/atlassian-agent.jar -L
    if [ ! -f "atlassian-agent.jar" ]; then
        echo 'atlassian-agent.jar download failed!'
        exit 1
    fi
fi

docker build -t confluence:8.5.2-JDK17-agent-chs .

