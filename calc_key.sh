#!/bin/bash

if [[ $# -eq 0 ]]; then
    echo "Usage: sh calc_key.sh <server-id>"
    exit 1
fi

docker exec -it confluence-8.5.2 java -jar /opt/atlassian/confluence/atlassian-agent.jar -s "$1" -p conf -m test@test.com -n test -o test