# docker-confluence-ubuntu

#### Description
docker版confluence镜像构建

#### Software Architecture

基于github `docker-atlassian-confluence` 项目定制而来. 

#### Installation

1.  制作公共镜像:
   1. 运行脚本:[build_confluence.sh](build_confluence.sh) 生成镜像 `confluence:8.5.2-JDK17`
2.  制作定制镜像: 
   1. 运行脚本:[build_confluence_extend.sh](docker-extend-image%2Fbuild_confluence_extend.sh) 生成镜像定制 `confluence:8.5.2-JDK17-agent-chs`
3.  运行启动脚本: 
   1. 运行脚本:[start_confluence.sh](start_confluence.sh) 启动镜像 `confluence:8.5.2-JDK17-agent-chs`
4.  访问地址: 
   1. 访问地址: `http://localhost:28080` 进行安装初始化
   2. 填写key: 执行脚本: [calc_key.sh](calc_key.sh),参数: `sh calc_key.sh <server-id>`
   3. 填写数据库信息: 
         1. 数据库类型: `mysql`
         2. 数据库地址: `jdbc:mysql://mysql-8.0.28:3306/conf_db?useUnicode=true&characterEncoding=utf8`
         3. 数据库端口: `3306`
         4. 数据库名称: `conf_db`
         5. 数据库用户名: `conf_user`
         6. 数据库密码: `conf_password`

#### Instructions

1.  本项目仅供测试使用, 不可用于生产环境.
2.  本项目不提供任何技术支持, 请自行解决问题.
3.  本项目不提供 agent 安装包, 请自行解决问题.
4.  其它更详细的参考文档见: [安装说明文档 - firfor.cn](https://www.firfor.cn/articles/2023/12/31/1703954570472.html) 或邮件交流. firfor@sina.con

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
